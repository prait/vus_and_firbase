<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
<div class="container" id="comment">
	<h2>praist deesuntia</h2>
	<!-- display comment -->
	<div class="card" v-for="message in messages">
		   <div class="card-body">
		   	 <h5 class="card-title" v-if="message!=editText">{{message.text}}</h5>
		   	 <textarea v-else name="name" class="form-control"  v-model="messageText2"></textarea>
		   	 <p class="card-text text-muted">{{message.name}}</p>
              <div v-if="message==confirmdel">
              	<a href="#" class="card-link" @click.prevent="deleteMessage(message)">ยินยัน</a>
              	<a href="#" class="card-link" @click.prevent="confirmcancleMessage(message)">ยกเลิก</a>
              </div>
              <div v-else>
                 <div v-if='message!=editText'>
                   <a href="#" class="card-link" @click.prevent="confirmMessage(message)">ลบ</a>
		   	       <a href="#" class="card-link" @click.prevent="editMessage(message)">แก้ไข</a>
                 </div>
                 <div v-else>
              	   <a href="#" class="card-link" @click.prevent="cancleMessage(message)">ยกเลิก</a>
		   	       <a href="#" class="card-link" @click.prevent="updateMessage(message)">อัพเดต</a>              	
                 </div>
              </div>
		   </div>
	</div>
	<div v-for="tnoti in notis">
	  <div id="statusid">{{tnoti.name}}</div>
	  

	  
	</div>
	<!-- form  design -->
	<hr>
	<form class="form-group" @submit.prevent="storeMessage">
	    <div>
	    	<textarea name="name" class="form-control"   v-model="messageText"></textarea>
	    </div>
	    <label for="">ชื่อ :</label>
	    <input type="text" class="form-control" name="" v-model="name"> <br>
	    <button type="submit" class="btn btn-primary">comment</button>
	</form>

</div>


<script src="//code.jquery.com/jquery.js"></script>
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js">
<!-- Bootstrap JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
<script>
	// var text = $("#statusid").text();
	// alert(text);

	// Swal.fire({
	// title: 'Error!',
	// text: 'Do you want to continue',
	// icon: 'error',
	// timer: 3000,
	// })

</script>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.5.0/firebase-database.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->

<script>
var audio = new Audio('noti.mp3');
</script>

<script>

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyCrllFich4y1ukGEjT67bZHZtTeJlJQQcI",
    authDomain: "vuejs-af7a5.firebaseapp.com",
    databaseURL: "https://vuejs-af7a5-default-rtdb.firebaseio.com",
    projectId: "vuejs-af7a5",
    storageBucket: "vuejs-af7a5.appspot.com",
    messagingSenderId: "381180710025",
    appId: "1:381180710025:web:970b90b6a281d775da0a58",
    measurementId: "G-2YGC2L5EJP"
  };

  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();


  
</script>
<script src="app.js"></script>
</body>
</html>